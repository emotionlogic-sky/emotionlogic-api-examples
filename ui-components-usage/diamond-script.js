import report from './data/clinton.json' with { type: "json" };

window.NMSDIAMOND.registerAllComponents();

const diamond = document.getElementById('nms-player');
diamond.setData({data: report});
diamond.setAttribute('video-url', './data/clinton.wav'); //video or audio url

