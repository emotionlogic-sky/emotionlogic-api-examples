import report from './data/clinton.json' with { type: "json" };

window.NMSDR.registerAllComponents();
window.NMSDIAMOND.registerAllComponents()

window.NMSDR.setProps('#nms-dr-speaker-distribution', {
	segments: report.segments
});

window.NMSDR.setProps('#nms-dr-emotion-player', {
	options: {
		files: ['/data/clinton.wav'],
		segments: report.segments,
		channel: 'both' // '0' or '1' or 'both'
	}
});

window.NMSDR.setProps('#nms-analysis-channel-report', {
	report: report,
	channel: 0, //0 or 1
});

