import report from './data/apptone.json' with {type: "json"};

const reportInfo = report.reportInfo;
const analysis = report.analysis;

const diamondSortOrder = [
	"EDP-Emotional",
	"EDP-Uneasy",
	"EDP-Stressful",
	"EDP-Thoughtful",
	"EDP-Confident",
	"EDP-Concentrated",
	"EDP-Energetic",
	"EDP-Passionate",
]

const edpText = {
	"EDP-Emotional": "Emotional",
	"EDP-Uneasy": "Uneasy",
	"EDP-Stressful": "Stressful",
	"EDP-Thoughtful": "Thoughtful",
	"EDP-Confident": "Confident",
	"EDP-Concentrated": "Concentrated",
	"EDP-Energetic": "Energetic",
	"EDP-Passionate": "Passionate"
}

const getDiamondJsonValues = () => {
	const edp = analysis.reports['channel-0']?.edp;
	if (!edp) return [];
	return JSON.stringify(diamondSortOrder.map(key => ({value: edp[key], text: edpText[key]})));
};

const getReportRiskValue = () => {
	const showRisk = !!reportInfo?.reportRiskIndicator && reportInfo?.reportRiskIndicator !== 'noRiskIndications';
	return showRisk ? analysis?.reports?.risk : undefined;
};


window.NMSDR.registerAllComponents();
window.NMSDIAMOND.registerAllComponents()

window.NMSDR.setProps('#nms-dr-title', {
	title: 'Apptone Report Demo',
	info: [
		{name: 'Report Name', value: reportInfo.questionnaireName},
		{name: 'Identifier', value: reportInfo.identifier},
		{name: 'Date', value: reportInfo?.createDate},
		{name: 'Time', value: reportInfo?.createTime},
	]
});

window.NMSDR.setProps('#nms-dr-conclusion', {
	risk: analysis.reports?.risk,
	gptConclusion: analysis.chatGptAnalysis?.conclusions,
	image: analysis.chatGptAnalysis.imageUrl,
	mainRiskField: reportInfo.reportRiskIndicator
});


window.NMSDR.setProps('#nms-diamond', {
	values: getDiamondJsonValues()
});

window.NMSDR.setProps('#nms-dr-channel-edp', {
	edp: analysis.reports['channel-0']?.edp
});

window.NMSDR.setProps('#nms-dr-risk-brief-topic-list-collapse', {
	topics: analysis.reports?.risk?.topics,
	mainRiskField: reportInfo.reportRiskIndicator,
	summaryQuestions: analysis.summary?.questions
});

window.NMSDR.setProps('#nms-dr-risk-brief-highlight-collapse', {
	riskQuestions: analysis.reports.risk?.questions,
	mainRiskField: reportInfo.reportRiskIndicator,
	questions: report.questions,
	segments: analysis.segments,
	hesitation: analysis.reports['channel-0'].profile.hesitation,
	clStress: analysis.reports['channel-0'].profile.clStress,
	aggression: analysis.reports['channel-0'].profile.aggression,
	overallCognitiveActivity: analysis.reports['channel-0'].profile.overallCognitiveActivity
});

window.NMSDR.setProps('#nms-dr-topic-key-strength-weakness-list-collapse', {
	strengthsWeaknesses: analysis.reports.competenciesCoreValues.strengthsWeaknesses,
	questions: report.questions,
	summaryQuestions: analysis.summary.questions
});

window.NMSDR.setProps('#nms-dr-topic-list', {
	risk: getReportRiskValue(),
	mainRiskField: reportInfo.reportRiskIndicator,
	competenciesCoreValues: analysis.reports.competenciesCoreValues,
	segments: analysis.segments,
	questions: report.questions,
	summaryQuestions: analysis.summary.questions,
	warnings: report.warnings
});

window.NMSDR.setProps('#nms-dr-stress-profile', {
	channelSegmentsCount: analysis.technical.segmentsCount,
	stress: analysis.reports['channel-0'].profile.stress,
	clStress: analysis.reports['channel-0'].profile.clStress,
	testReport: analysis.reports['channel-0'].testReport
})

window.NMSDR.setProps('#nms-dr-mood-profile', {
	aggression: analysis.reports['channel-0'].profile.aggression,
	joy: analysis.reports['channel-0'].profile.joy,
	sad: analysis.reports['channel-0'].profile.sad,
	showEmotionDistribution: false
});

window.NMSDR.setProps('#nms-dr-behavioral-profile', {
	anticipation: analysis.reports['channel-0'].profile.anticipation,
	concentration: analysis.reports['channel-0'].profile.concentration,
	hesitation: analysis.reports['channel-0'].profile.hesitation
});

window.NMSDR.setProps('#nms-dr-emotional-profile', {
	arousal: analysis.reports['channel-0'].profile.arousal,
	excitement: analysis.reports['channel-0'].profile.excitement,
	uneasy: analysis.reports['channel-0'].profile.uneasy
});

window.NMSDR.setProps('#nms-dr-logical-profile', {
	mentalEfficiency: analysis.reports['channel-0'].profile.mentalEfficiency,
	imagination: analysis.reports['channel-0'].profile.imagination,
	mentalEffort: analysis.reports['channel-0'].profile.mentalEffort,
	uncertainty: analysis.reports['channel-0'].profile.uncertainty
});

window.NMSDR.setProps('#nms-dr-atmosphere-profile', {
	atmosphere: analysis.reports['channel-0'].profile.atmosphere,
	discomfort: analysis.reports['channel-0'].profile.discomfort
});

window.NMSDR.setProps('#nms-dr-speaker-distribution', {
	segments: analysis.segments
});

window.NMSDR.setProps('#nms-dr-emotion-player', {
	options: {
		files: ['/data/clinton.wav', '/data/clinton.wav'],
		segments: analysis.segments,
		channel: 'both' // '0' or '1' or 'both'
	}
});

window.NMSDR.setProps('#nms-dr-call-tags', {
	tags: analysis.reports['channel-0'].tags
});

window.NMSDR.setProps('#nms-dr-technical-info', {
	technical: analysis.technical,
	inlineStyle: true,
});

const afterLoad = () => {
	const diamonds = [...document.getElementsByTagName('nms-diamond')];
	diamonds.forEach(item => {
		const style = document.createElement('style');
		style.innerHTML = `
      .emlo-diamond {width: 380px; height: 251px;}
      text.emlo-diamond__svg-text { fill: #45557D; }
      svg path[fill-opacity="0.15"]{
          stroke: #d9deec;
          fill: #eff2f9;
      }
    `;
		item.shadowRoot.appendChild(style);
	})

	const moodProfiles = [...document.getElementsByTagName('nms-dr-mood-profile')];
	moodProfiles.forEach(item => {
		const style = document.createElement('style')
		style.innerHTML = '.four-lvl-pie-container .chart > * {width: 170px; height: 170px;}';
		item.shadowRoot.appendChild(style)
	})
}
setTimeout(afterLoad, 100);