
const EmotionLogicAppToneAPI = require('./EmotionLogicAppToneAPI');

const baseURL = 'https://api.emlo.cloud/apigateway-service/apptone/v1';
const apiUser = '[API KEY]';
const apiPassword = '[API KEY PASSWORD]';
const webhookUrl = '[WEBHOOK_URL]';
const reportId = '[REPORT_ID]';

const api = new EmotionLogicAppToneAPI(baseURL, apiUser, apiPassword);

async function main() {

    try {

        const questionairId = '7c834d22-78e9-4a83-9a7b-9bd3cd1cf21d';
        const sendResult = await api.sendToCustomer(questionairId, '+972545615770', 'Yariv Goren', webhookUrl);
        console.log('Send to Customer Result:', sendResult);


        const pdfData = await api.downloadPdf(reportId, webhookUrl);
        console.log('PDF Data:', pdfData);

        const cancelResult = await api.cancel(reportId);
        console.log('Cancel Result:', cancelResult);

        const questionnaires = await api.getQuestionnaires('test', ['car', 'www'], ['es', 'en']);
        console.log('Questionnaires:', questionnaires);
    } catch (error) {
        console.error(error);
    }
}

main();
