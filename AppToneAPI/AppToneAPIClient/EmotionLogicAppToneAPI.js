
const axios = require('axios');


class EmotionLogicAppToneAPI {
    constructor(baseURL, apiUser, apiPassword) {
        this.client = axios.create({
            baseURL: baseURL,
            auth: {
                username: apiUser,
                password: apiPassword,
            },
        });
    }

    async downloadPdf(reportId, webhookUrl) {
        const url = `/reports/${reportId}/downloadPdf`;
        const body = { statusCallbackUrl: webhookUrl };
        try {
            const response = await this.client.post(url, body);
            return response.data;
        } catch (error) {
            throw new Error(`Error in downloadPdf: ${error.message}`);
        }
    }

    async sendToCustomer(questionairId, phoneNumber, name, webhookUrl) {
        const url = `/questionnaires/${questionairId}/sendToCustomer`;
        const body = { phoneNumber, name, statusCallbackUrl: webhookUrl };
        try {
            const response = await this.client.post(url, body);
            return response.data;
        } catch (error) {
            throw new Error(`Error in sendToCustomer: ${error.message}`);
        }
    }

    async cancel(reportId) {
        const url = `/reports/${reportId}/cancel`;
        const body = {};
        try {
            const response = await this.client.post(url, body);
            return response.data;
        } catch (error) {
            throw new Error(`Error in cancel: ${error.message}`);
        }
    }

    async getQuestionnaires(query, tags, languages) {
        const url = '/questionnaires';
        const params = { query, tags, languages };
        try {
            const response = await this.client.get(url, { params });
            return response.data;
        } catch (error) {
            throw new Error(`Error in getQuestionnaires: ${error.message}`);
        }
    }
}

module.exports = EmotionLogicAppToneAPI;
