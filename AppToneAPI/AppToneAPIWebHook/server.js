const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

// Middleware to parse JSON bodies
app.use(bodyParser.json());

/*
    Success message structure

    {
        "requestId": "11111111-1111-1111-11111-111111111111",
        "application": "apptone",
        "eventDate": "2024-06-19T13:40:46.079Z",
        "payload": {
        "reportId": "11111111-1111-1111-11111-111111111111",
            "status": "pending/running/analyzing/completed"
    },
        "encrypted": false
    }

 */


/*
    Error message structure

    {
        "requestId": "11111111-1111-1111-11111-111111111111",
        "application": "apptone",
        "eventDate": "2024-06-19T13:40:46.079Z",
        "payload": {
        "reportId": "11111111-1111-1111-11111-111111111111",
        "error": "There is already a running test for this phone number.",
        "errorCode": "invalid_parameter"
    },
        "encrypted": false
    }

*/


app.post('/webhook', (req, res) => {
    const { requestId, application, eventDate, payload, encrypted } = req.body;

    // Validate the "application" value
    if (application !== 'apptone') {
        console.log(`Invalid application: ${application}`);
        return res.status(400).send('Invalid application');
    }

    // Check for "errorCode" attribute in "payload"
    if (payload.errorCode) {
        console.log(`Error received: ${payload.error}`);
        return res.status(400).send('Error in payload');
    }

    console.log('Received valid webhook:', req.body);

    /*
        Do business logic here by the test status that may be one of the following:
        pending/running/analyzing/completed
     */

    if(payload.status === "completed"){
        // you may want to download the report PDF here by calling "downloadpdf" endpoint
    }
    

    // Respond to acknowledge receipt of the webhook
    res.status(200).send('Webhook received');
});

app.listen(port, () => {
    console.log(`Server listening on port ${port}`);
});
