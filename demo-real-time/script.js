const ANALYSIS_SERVER_URL = "ws://localhost:2259";

const audioSetting = {
  isPCM: true,
  channels: 1,
  bitRate: 16,
  sampleRate: 11025,
  backgroundNoise: 1000,
};

const AUDIO_PACKET_SIZE_MS = 300;

const bufferSize = Math.round(audioSetting.sampleRate / 1000 * AUDIO_PACKET_SIZE_MS); // 3308 = 300ms of audio at 11025Hz

let socket;
let audioContext;


// this audio format use by the docker for analysis.
// we force the input from the microphone to be
// in this format


// Attach event listeners to buttons
document.getElementById("startBtn").addEventListener("click", () => {
  startAudioAnalysis();
  document.getElementById("startBtn").disabled = true; // Disable start button while recording
  document.getElementById("stopBtn").disabled = false; // Enable stop button
});

document.getElementById("stopBtn").addEventListener("click", stopAudioAnalysis);


async function startAudioAnalysis() {

  socket = null;
  try {
    // connect to the docker
    socket = await connect(ANALYSIS_SERVER_URL)
    socket.on("audio-analysis-error", (err) => {
      throw err;
    });

    // making a handshake to the docker
    await handshake();

    // set functions to handle analysis events
    await analyzeStream();

    // Start recording from device microphone, and stream to the docker
    await startRecording();

  } catch (err) {
    console.log("Initialization error:", err)
    // on error, disconnect from docker and reset values
    stopAudioAnalysis();
    throw err;
  }
}

async function connect(url) {

  return new Promise((resolve, reject) => {
    const socket = io.connect(url, {
      transports: ["websocket"]
    });

    socket.on("connect", () => {
      console.log("Connected to analysis server.");
      resolve(socket)
    });

    socket.on("connect_error", (err) => {
      console.log("Error connecting to analysis server:", err);
      stopAudioAnalysis();
      reject(err);
    });
  });
}

function handshake() {

  return new Promise((resolve, reject) => {
    const onHandshakeDone = r => {
      socket.off("handshake-done", onHandshakeDone);
      if (r.success) {
        resolve(r.data);
      } else {
        reject(new Error(r?.error || "Unexpected error occurred on handshake"));
      }
    };

    socket.on("handshake-done", onHandshakeDone);

    socket.emit("handshake", {
      isPCM: audioSetting.isPCM,
      channels: audioSetting.channels,
      backgroundNoise: audioSetting.backgroundNoise,
      bitRate: audioSetting.bitRate,
      sampleRate: audioSetting.sampleRate,
      sensitivity: "normal", // normal/high/low
      // 'json' is the default value, so it is nota mandatory parameter
      outputType: 'json',
    });
  });
}

async function analyzeStream() {
  // Handle response from the docker
  socket.on("audio-analysis", (response) => {
    if (response.success) {
      if (response.data.done) {
        // response.data.done = true means that the segment is ready
        // response.data.done = false is a valid response,
        // it means that the segment isnot ready yet
        console.log("Analysis complete: ", JSON.stringify(response.data));
        let div = document.createElement("div");
        div.append(JSON.stringify(response.data));
        document.getElementById("response").appendChild(div);
      }
    } else {
      console.error("Error in audio analysis: ", response.error);
    }
  });

  socket.on("analysis-report", (report) => {
    if (report.success) {
      // the analysis report is ready
      console.log("Analysis Report: ", report.data);
    } else {
      // generating analysis report failed
      console.error("Failed to fetch analysis report: ", report.error);
    }
  });
}

async function startRecording() {
    // Step 1: Get access to the microphone
    const stream = await navigator.mediaDevices.getUserMedia({ audio: true });

    // Step 2: Create an AudioContext with the desired sample rate
    audioContext = new AudioContext({ sampleRate: audioSetting.sampleRate });

    // Step 3: Connect the media stream to the AudioContext
    const source = audioContext.createMediaStreamSource(stream);

    // Step 4: Create an AudioWorklet to process the audio
    await audioContext.audioWorklet.addModule(URL.createObjectURL(new Blob([`
        class PCMProcessor extends AudioWorkletProcessor {
            constructor() {
                super();
                this.buffer = [];
                this.bufferSize = ${bufferSize}; // 3308 samples for 300ms
            }

            process(inputs, outputs, parameters) {
                const input = inputs[0];
                if (input.length > 0) {
                    const inputChannelData = input[0]; // Assuming single channel (mono) input
                    for (let i = 0; i < inputChannelData.length; i++) {
                        this.buffer.push(inputChannelData[i]);
                        if (this.buffer.length >= this.bufferSize) {
                            this.sendBuffer();
                        }
                    }
                }
                return true;
            }

            sendBuffer() {
                // convert the audio to PCM, bitRate - 16
                const int16Buffer = new Int16Array(this.buffer.length);
                for (let i = 0; i < this.buffer.length; i++) {
                    int16Buffer[i] = Math.max(-1, Math.min(1, this.buffer[i])) * 0x7FFF; // Convert float to 16-bit PCM
                }

                this.port.postMessage(int16Buffer);
                this.buffer = []; // Clear buffer for the next 300ms
            }
        }
        registerProcessor('pcm-processor', PCMProcessor);
    `], { type: 'application/javascript' })));

    const pcmNode = new AudioWorkletNode(audioContext, 'pcm-processor');

    // Listen for messages from the processor (every 300ms)
    pcmNode.port.onmessage = (event) => {

        const int16Buffer = event.data;
        sendSamples(int16Buffer);
    };

    source.connect(pcmNode).connect(audioContext.destination);
}

async function sendSamples(data) {
  // send the audio buffer to the docker
  socket.emit("audio-stream", data);
}

function stopAudioAnalysis(){
  if(socket){
    // send zero-lengs buffer to indicate end of stream
    socket.emit("audio-stream", new Int16Array(0));
    socket.disconnect();
    socket = null;
  }

  audioContext?.close();

  document.getElementById("startBtn").disabled = false;
  document.getElementById("stopBtn").disabled = true;
  document.getElementById("response").innerHTML = '';
}