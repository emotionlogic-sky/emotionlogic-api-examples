/*

    This example demonstrates real-time analysis (streaming) API.
    On a real-world situation, that audio will come from switchboard.
    On this example we read a file and stream is as it was an audio comming from the switchboard.

    The analysis algoritm uses 11025 samples per second, and 16 bits per sample, PCM, therefore this is the recommencded
    audio format.

    In case a different sample-rate streamed, the docker convert the original format to 11025, and this process slows the analysis process by about 9 times.
*/

const wavefile = require("wavefile");
const fs = require("fs");
const io = require("socket.io-client");

const FILE_PATH = "./clinton_11025.wav"
const ANALYSIS_SERVER_URL = "ws://localhost:2259"; // docker IP and port
const AUDIO_PACKET_SIZE_MS = 300; // set 300 ms packets to the docker


streamFile(ANALYSIS_SERVER_URL, FILE_PATH, "json")

function streamFile(analysisServerUrl, inputFilePath, outputType = "json") {

    const wav = new wavefile.WaveFile();
    const buffer = fs.readFileSync(inputFilePath)

    wav.fromBuffer(buffer);

    if (wav.fmt.bitsPerSample % 8 !== 0) {
        throw new Error("invalid bitsPerSample");
    }

    const socket = io.connect(analysisServerUrl, {
        transports: ["websocket"]
    });


    socket
        .on("connect", () => sendHandshake(socket, wav, outputType))
        .on("connect_error", (err) => {
            throw err
        })
        .on("audio-analysis-error", (err) => {
            throw err
        })
        .on("handshake-done", (r) => sendSamples(socket, wav))
        .on('audio-analysis', (r) => processAnalysisData(socket, r))
        .on("analysis-report-ready", (r) => analysisReportReady(socket, r))
        .on("audio-analysis-completed", (r) => {

            if (!r.success) {
                throw new Error(r?.error || "Unexpected error occurred on finalize");
            }

            fetchAnalysisReport(socket);
        })
}

const fetchAnalysisReport = (socket) => {

    socket.emit("fetch-analysis-report", {
        // return report on json format
        outputFormat: "json",
        // true -> return on the report all the segments in the call.
        // in that case, the result will be similar to an offline analysis
        fetchSegments: true,
    });
}

const analysisReportReady = (socket, r) => {

    if (!r.success) {
        throw new Error(r?.error || "Unexpected error occurred on finalize");
    }

    console.log(JSON.stringify(r));

    socket.disconnect();
};

const processAnalysisData = (socket, {success, data}) => {

    if (!success) {
        throw new Error(data.error);
    }

    if (data.done) {
        console.log(JSON.stringify(data) + "\r\n");
    }
}

const sendHandshake = (socket, wav, outputType) => {

    socket.emit("handshake", {
        isPCM: wav.fmt.audioFormat === 1,
        channels: wav.fmt.numChannels,
        backgroundNoise: 1000,
        bitRate: wav.fmt.bitsPerSample,
        sampleRate: wav.fmt.sampleRate,
        codec: wav.fmt.audioFormat === 7 ? "mulaw" : "",
        sensitivity: "normal", // normal/high/low
        outputType,
    });
}

function calcPacketSizeBytes(wav, audioLengthMS){

    // this function assumes bitsPerSample = 16 or 8 ==> the 2 supported values by the docker

    const singleSampleTimeMS = 1000 / wav.fmt.sampleRate;
    const singleSampleSizeBytes = wav.fmt.bitsPerSample / 8 * wav.fmt.numChannels;

    // Need to round to integer, as audioLengthMS / singleSAmpleTimeMS most likely will not be an integer
    const requiredSamplesCount = Math.round(audioLengthMS / singleSampleTimeMS);

    const packetSizeBytes = requiredSamplesCount * singleSampleSizeBytes;

    return packetSizeBytes;
}

const sendSamples = (socket, wav) => {

    let packetSize = calcPacketSizeBytes(wav, AUDIO_PACKET_SIZE_MS);
    let offset = 0;

    const samples = wav.data.samples;

    const interval = setInterval(() => {

        if (!socket.connected) {
            throw new Error("Socket is not connected");
        }

        const arraySize = Math.min(packetSize, samples.byteLength - offset);

        if (arraySize) {

            socket.emit("audio-stream", samples.slice(offset, offset + arraySize));
            offset += arraySize;

        } else {
            socket.emit("audio-stream", Buffer.alloc(0));
            clearInterval(interval);
        }

    }, AUDIO_PACKET_SIZE_MS);
}
