
This sample demonstrate streamning to the docker.
On the real world, the audio comes from the call center. Since this code do not have a "call center", we read an audio file and stream it.



Best practice for streaming an audio to the docker 

1. Asseble small packets arrived fron the call center to a buffer of 300ms. For example, if the packets from the call center are 20ms each, collect 15 packets to build a single 300ms buffer, then send it to the docker
2. The docker run analysis on the following audio format: sample depth - 16 bits, sample rate - 11025, PCM WAV 
3. For best performance, convert the audio from the call center to the above format. 
4. In case the streamed audio format is not as mentioned above, the docker is capable of converting the audio format.
5. Make sure you pass the accurate audio format, es demonstrated on "sendHandshake" function in the sample code


Running the sample

npm install
node stream-analysis-sample-no-promises.js

 
