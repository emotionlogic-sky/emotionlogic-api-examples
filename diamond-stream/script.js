import report from './data/clinton.json' with { type: "json" };

window.NMSDIAMOND.registerAllComponents();

const diamondHeadersDictionary = {
  'EDP-Emotional': 'emotional',
  'EDP-Uneasy': 'uneasy',
  'EDP-Stressful': 'stressful',
  'EDP-Thoughtful': 'thoughtful',
  'EDP-Confident': 'confident',
  'EDP-Concentrated': 'concentrated',
  'EDP-Energetic': 'energetic',
  'EDP-Passionate': 'passionate',
}

const segments = report.segments.data;
const headersPositions = report.segments.headersPositions;

let segmentId = 0;
const maxSegmentId = segments.length - 1;


const interval = setInterval(() => {
  if(segmentId < maxSegmentId) segmentId++;
  else clearInterval(interval);
  
  window.NMSDIAMOND.setProps('#nms-diamond', {
    values: segmentData(segments[segmentId]),
  });
}, 1000);



function segmentData(segment){
  const report = [];

  for (const dk in diamondHeadersDictionary) {
    const value = segment[headersPositions[dk]];
    report.push({
      text: diamondHeadersDictionary[dk],
      value: value,
    })
  }

  return report;
}